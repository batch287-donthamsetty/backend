db.users.insertOne({
	name: "Single",
	accommodates: "2",
	price: "1000",
	description: "A simple room with all the basic necessities",
	rooms_avaialble: "10",
	isAvailable: "false"
});

db.users.insertMany ([
{
	name: "Double",
	accommodates: "3",
	price: "2000",
	description: "A room fit for a small family going on a vacation",
	rooms_avaialble: "5",
	isAvailable: "false"
	}
]);

	
db.users.insertMany ([
{
	name: "Queen",
	accommodates: "4",
	price: "4000",
	description: "A room with a queen sized bed perfect for a simple getaway",
	rooms_avaialble: "15",
	isAvailable: "false"
	}
]);

db.users.updateOne(
        { rooms_avaialble: "15" },
        {
            $set: {
              name: "Queen",
		accommodates: "4",
		price: "4000",
		description: "A room with a queen sized bed perfect for a simple getaway",
		rooms_avaialble: "0",
		isAvailable: "false"  
                
            }
        }
    );

 db.collectionName.deleteMany({ rooms_avaialble: "0" });
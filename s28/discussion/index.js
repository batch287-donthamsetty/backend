// CRUD Operations
	// CRUD Operations are the heart of any backend application.
	// Mastering the CRUD Operations is essential for any developer.

// [ SECTION ] Inserting Documents (CREATE)
// Insert One Documents
	// Creating MongoDB Syntax in a text editor makes it easy for us to modify and create our code as opposed to typing it directly.
	//Syntax:
		// db.collectionName.insertOne({Object})

db.users.insertOne ({
    "firstName": "John",
    "lastName": "Smith"
});

db.users.insert({
	firstName:"John",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "1234567890",
		email: "janedoe@mail.com"
	},
	courses: ["CSS", "Javascript", "Phyton"],
	department: "none"
});

// Insert Many Document

db.users.insertMany ([
{
	firstName: "Stephen",
	lastName: "Hawking",
	age: 76,
	contact: {
		phone: "1234567890",
		email: "theoryofeverything@mail.com"
	},
	courses: ["Phyton", "React", "PHP"],
	department: "none"
},
{
	firstName: "Neil",
	lastName: "Armstrong",
	age: 82,
	contact: {
		phone: "1234567890"
		email: "firstmaninthemoon@mail.com"
	},
	courses: ["React", "Laravel", "Sass"],
	department: "none"
	}
]);

// [ SECTION ] Finding Documents (READ)
	// Find
		// If multiple documents match the criteria for finding a document only the FIRST document that matches search term will be returned
	// Syntax
		db.collectionName.find();
		db.collectionName.find({ field: value});
	db.users.find();
	db.users.find({ firstName: "Stephen"});

// [ SECTION ] Updating Documents (UPDATE)

    // Updating a Single Document

    db.users.insertOne({
        firstName: "Test",
        lastName: "Test",
        age: 0,
        contact: {
            phone: "0000000000",
            email: "test@mail.com"
        },
        course: [],
        department: "none"
    });

    // To update firstName: "Test"
    // Syntax:
        db.collectionName.updateOne( {criteria}, { $set { field: value }});
    db.users.updateOne(
        { firstName: "Test"},
        {
            $set: {
                firstName: "Bill",
                lastName: "Gates",
                age: 65,
                contact: {
                    phone: "0987654321",
                    email: "billgates@mail.com"
                },
                course: ["PHP", "C++", "HTML"],
                department: "Operations",
                status: "active"
            }
        }
    );

    // Update Many

    //Syntax:
        //db.collectionName.updateOne( {criteria}, { $set { field: value }});
    db.users.updateMany(
        { department : "none" },
        {
            $set: { department: "HR" }
        }
    );

// [ SECTION ] Deleting Documents (DELETE)

    // Delete One

        // Syntax:
            db.collectionName.deleteOne({ criteria });

    db.users.insertOne({
        firstName: "Test"
    });

        db.users.deleteOne({
            firstName: "Test"
        });

    // Delete Many
        // Syntax
            db.collectionName.deleteMany({ criteria });
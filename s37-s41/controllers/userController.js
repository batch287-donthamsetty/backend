const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	
	return User.find({ email: reqBody.email }).then(result => {
		
		// The "find" method returns a record if a match is found
		if(result.length > 0){
			
			return true
		
		// No duplicate email found
		// The user is not yet registered in the database
		} else {
			
			return false
		
		}
	})
}

module.exports.registerUser = (reqBody) => {

	console.log(reqBody);

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {

		if(error){
			return false
		} else {
			return true
		}
	})
}

module.exports.loginUser = (reqBody) => {

	// .findOne() will look for the first document that matches
	return User.findOne({ email: reqBody.email }).then(result => {

		console.log(result);

		// Email doesn't exist
		if(result == null){

			return false;

		} else {

			// We now know that the email is correct, is password correct also?
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password); //true / false

			console.log(isPasswordCorrect);
			console.log(result);

			// Correct password
			if(isPasswordCorrect){

				return { access: auth.createAccessToken(result) }

			// Password incorrect	
			} else {

				return false
				
			};

		};
	});
};

module.exports.getProfile = (data) => {

	console.log(data); // Will return the key/value of pair of userId: data.Id

	// Changes the value of the user's password to an empty string when returned to the frontend
	// Not doing so will expose the user's password which will also not be needed in other parts of our application
	// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application

	return User.findById(data.userId).then(result => {
		result.password = "";
		// Returns the user information with the password as an empty string
		return result;
	})
}

// Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user
module.exports.enroll = async (data) => {
	
	// Add the course ID in the enrollments array of the user
	// Creates an "isUserUpdated" variable and returns true upon successful update otherwise false
	// Using the "await" keyword will allow the enroll method to complete updating the user before returning a response back to the frontend
	// 1st Await Update first the User Models
	let isUserUpdated = await User.findById(data.userId).then(user => {
		// Adds the courseId in the user's enrollments array
		user.enrollments.push({ courseId: data.courseId });
		// Saves the updated user information in the database
		return user.save().then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			};
		});
	});

	// Add the user ID in the enrollees array of the course
	// Using the "await" keyword will allow the enroll method to complete updating the course before returning a response back to the frontend
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		// Adds the userId in the course's enrollees array
		course.enrollees.push({ userId: data.userId });
		// Saves the updated course information in the database
		return course.save().then((course, error) => {
			if(error){
				return false;
			} else {
				return true;
			};
		});
	});

	// Condition that will check if the user and course documents have been updated
	// User enrollment successful
	if(isUserUpdated && isCourseUpdated){
		return true;
	// User enrollment failure
	} else {
		return false;
	};
};
// [ SECTION ] Comparison Query Operators

// $gt/$gte

// Allows us to find documents that have field number values greater than or equal to a specified value.
db.users.find({ age : { $gt : 50} }).pretty();
db.users.find({ age : { $gte : 50} }).pretty();

// $lt/$lte

// Allows us to find documents that have field number values less than or equal to a specified value.
db.users.find({ age : { $lt : 50} }).pretty();
db.users.find({ age : { $lte : 50} }).pretty();

// $ne

// Allows us to find documents that have field values that are not equal to a specified value
db.users.find({ age : { $ne : 82} }).pretty();

// $in

// Allows us to find documents with specific match criteria using different values

db.users.find({ lastName: { $in: ["Hawking", "Doe"]}}).pretty();
db.users.find({ courses: { $in: ["HTML", "React"]}}).pretty();

// [Section] Logical Query Operators

// $or operator

// Allows us to find documents that match a signle criteria from multiple provded search criteria

db.users.find({ $or: [ { firstName: "Neil"}, {age: 25}]}).pretty();

db.users.find({ $or: [ { firstName: "Neil"}, {age: { $gt: 30} } ] } ).pretty()

// $and operator

//  Allows us to find documents matching multiple criteria in a single field

db.users.find({ $and: [{ age: { $ne: 82}}, {age: { $ne: 76 } } ] } ).pretty()

// [Section] Field Projection
/*
	- Retrieving documents are common operations that we do and by default MongoDB queries return the whole document as a response
	- When dealing with complex data structures, there might be instances when fields are not useful for the query we are trying to achieve
	- To help with readability of the values returned, we can include/exclude fields from the response
*/

// Inclusion
/*
	-Allows us to include/add specific fields only when retrieving documents
	-The value provided is 1 to denote that the field is being included
*/

db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1
	}
).pretty()

// Exclusion
/*
	-Allows us to exclude/remove specific fields only when retrieving documents
	-The value provided is 0 to denote that the field is being included
*/

db.users.find(
	{
		firstName: "Jane"
	},
	{
		lastName: 0,
	}
).pretty()

// Supressing the ID field
/*
	-Allows us to exluce the "_id" field when retrieving documents
	-When using field projection, field inclusion and exclusion may not be used at the same time
	-Excluding the "_id" field is the only exception to this rule
*/

db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		_id: 0
	}
).pretty()

//Returning Specific fields in embedded documents
db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		"contact.phone": 1
	}
).pretty()

// Supressing specific fields in embedded documents
db.users.find(
	{
		firstName: "Jane"
	},
	{
		"contact.phone": 0
	}
).pretty()

// Project Specific Array Elements in the Returned Array
// the $slice operator allows us to retrieve only 1 element that matches the search criteria
db.users.find(
	{ "namearr":
		{
			nameb: "tamad"
		}

	},
	{ namearr:
		{
			$slice: 1
		}

	}
).pretty()

// [Section] Evaluation Query Operators

// $regex operator
/*
	-Allows us to find documents that match a specific string pattern using regular expressions
*/

// Case sensitive query
db.users.find({ firstName: { $regex: 'N'}}).pretty()

// Case insensitive query
db.users.find({ firstName: { $regex: 'j', $options: 'i' } }).pretty();
const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.zejnk78.mongodb.net/s35-activity", {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database."));

const signupSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  status: {
    type: String,
    default: "pending"
  }
});

const signup = mongoose.model("signup", signupSchema);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.post("/signup", (req, res) => {
  signup.findOne({ username: req.body.username }).then((result, err) => {
    if (result != null && result.username == req.body.username) {
      return res.send("Duplicate user found");
    } else {
      let newSignup = new signup({
        username: req.body.username,
        password: req.body.password
      });
      newSignup.save().then((savedSignup, saveErr) => {
        if (saveErr) {
          return console.error(saveErr);
        } else {
          return res.status(201).send("New user created!");
        }
      });
    }
  });
});

app.get("/signup", (req, res) => {
  signup.find({}).then((result, err) => {
    if (err) {
      return console.log(err);
    } else {
      return res.status(200).json({
        data: result
      });
    }
  });
});

app.listen(port, () => console.log(`Server running at port ${port}`));
const express = require("express");
// Mongoose is a package that allows creation of Schema to mode our data structure
// Also has access to a number of methods for manipulating our databse
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// [ SECTION ] MongoDB Connection

	// mongoose.connect("<MongoDB connecting string>", { userNewUrlParser: true });

// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.tkxmolv.mongodb.net/s35-discussion", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

	// The { newUrlParses : true } allows us to avoid any current and futures errors while connecting to MongoDB.
	// The { useUnifiedTopogy : true } allos us to enable the new MongoDB connection with the configuration option while establising MongoDB server and Mongoose.

// Connection to the Database	
	
	// Allows us to handle errors when the initial connection is established

let db = mongoose.connection

// If a connection error occured, output in the console.
// console.error.bind(console) allows us to print errors in the browser console and in the terminal
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database."));

// [ SECTION ] Mongoose Schemas

	// Schemas determine the structure of the docuements to be written in the database
	// Schemas acts as blueprint to our data
	// Use the schema() constructor of the Mongoose module to create a new Schema object
	// The "new" keyword creates a new Schema

const taskSchema = new mongoose.Schema({
	// Define the fields with the corresponding data type
	// For a task, it needs a "task name" and "task status"
	// There should a field called "name" and its data type is "String"
	name: String,
	status: {
		type: String,
		// Default values are the pre-defined values for a field if we don't put any value
		default: "pending"
	}
})

// [ SECTION ] Models

	// It uses schemas and are used to create/instantiate objects that correspond to the schema
	// It acts like as a middleman from ther server (JS CODE) to our database
	// Server > Schema (Blueprint) > Database > Collection

const Task = mongoose.model("Task", taskSchema);

// [ SECTION ] Creation of todo list routes

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

	// Creating a new Task
	
	// Business Logic

	/*
		1. Add a functionality to check if there are duplicate task
			- If the task already exists in the database, we return an error
			- If the task doesn't exist in the database, we add it in the database
		2. The task data will be coming from the request's body
		3. Create a new Task object with a "name" field/property
		4. The "status" property does not need to be provided because our schema by default is for it to be "pending"
	*/

app.post("/tasks", (req, res) => {

	// Checks if there are duplicate tasks
	// "findOne" is a mongoose method that acts similar to "find" or MongoDB
	// "findOne()" return the first document that matches the search criteria as single object
	Task.findOne({name : req.body.name}).then((result, err) => {

		// If a document was found and the document's name matches the information sent via the client/postman
		if(result != null && result.name == req.body.name){

			// Return a message to the client/Postman
			return res.send("Duplicate task found");

		// If no document was found
		} else {

			// Create a new task and save it to the database
			let newTask = new Task({
				name: req.body.name
			});

			// "save()" method will store the information to the database
			newTask.save().then((savedTask, saveErr) => {

				// If there are errors in saving
				if(saveErr){

					// Will print any errors found in the console
					return console.error(saveErr);

				// No error found while creating the document
				} else {

					// Return this to our cliend/Postman
					return res.status(201).send("New task created!");

				}
			})
		}
	})
})

// Getting all the task

	// Business Logic

	/*
		1. Retrieve all the documents
		2. If an error is encountered, print error
		3. If no error are found, send a success status back to the client/Postman and return an array of documents
	*/

app.get("/tasks", (req, res) => {

	// "find" is a Mongoose Method that is similar to MongoDB 'find', and empty "{}" means it returns all documents and stores them in the "result" parameter of the callback function
	Task.find({}).then((result, err) => {

		// If an error occured
		if(err){

			// To print any errors found in the console
			return console.log(err);

		// If no error are found	
		} else {

			// Return the result
			// The "json" method allows to send a JSON format for the response
			// The returned response is purposefully returned as an object with the "data" property to mirror the real world complex data structures

			return res.status(200).json({
				data: result
			})
		}
	})
})

app.listen(port, () => console.log(`Server running at port ${port}`));
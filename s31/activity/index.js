// 1. What directive is used by Node.js in loading the modules it needs?

	// Answer: Require directive.

// 2. What Node.js module contains a method for server creation?

	// Answer: The HTTP module.

// 3. What is the method of the http object responsible for creating a server using Node.js?

	// Answer: createServer().

// 4. What method of the response object allows us to set status codes and content types?
	
	// Answer: setStatus method.

// 5. Where will console.log() output its contents when run in Node.js?

	// Answer: The web console.

// 6. What property of the request object contains the address's endpoint?

	// Answer: Url property.

// Node.js

const http = require('http');

// Creates a variable 'port' to store the port number
const port = 3000;

// Creates a variable 'server' that stores the output of the 'createServer' method
const server = http.createServer((request, response) => {

	// Accessing the 'login' routes returns a message of "Welcome to the server! This is currently running at local host: 4004."
	if (request.url === '/login'){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to the login page.')

	// All other routes will return a message of "Error"
	} else {

		// Set a status code for the response - a 404 means Not Found
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end("I'm sorry the page you are looking for cannot be found.")
	}
})

server.listen(port);

console.log(`Server is successfully running at localhost:${port}.`);
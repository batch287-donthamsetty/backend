const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require ("./routes/taskRoute")
const app = express();
const port = 3000;
app.use(express.json());
app.use(express.urlencoded({extended: true}));
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.zejnk78.mongodb.net/s36-discussion",
{
    useNewUrlParser : true,
    useUnifiedTopology :true
});

mongoose.connection.once("open", ()=>console.log("We're connected to cloud database"));

app.listen(port,() => console.log(`Server running at port ${port}`));
app.use("/tasks",taskRoute);


const express = require("express");
const router =express.Router();
const taskController = require ("../controller/taskControllers");

// router.get("/",(req,res) =>{
//     taskController.getAllTasks().then(resultFromController => res.send(resultFromController));

// });

router.get("/:id",(req,res) =>{
    taskController.getSpecificTasks(req.params.id, req.body).then(resultFromController => res.send(resultFromController));

});


router.put("/:id/complete", (req,res) => {

	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));

});

module.exports = router;



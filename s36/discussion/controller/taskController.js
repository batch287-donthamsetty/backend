// Contains instructions on HOW your API will perform its intended tasks
// All of the operations can be done will being placed in this file 

const Task = require("../models/task");

// Controller for getting all the tasks
// Defines the function to be used in the "taskRoute.js" file and export these functions
module.exports.getAllTasks = () => {

	// The "return" statement, returns the result of the Mongoose method "find" back to the "taskRoutes.js"
	// The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to our client/Postman

	return Task.find({}).then(result => {
		// The 'return' sattement returns the result of the MongoDB query to the result parameter defined in the "then" method.
		return result;
	});
};

// Controller creating a task
// The request body coming from the client was passed from the "taskRoute.js" file via the "req.body" as an argument and is renamed as a "requestBody" parameter in the controller file.

module.exports.createTask = (requestBody) =>{

	// Create a task model based on the Mongoose model "Task"
	let newTask = new Task({

		// Set the "name" property with the value recieved from the client/Postman
		name: requestBody.name
	})

	// Saves the newly created "newTask" object in the MongoDB database 
	// The "then" method wait until task is stored in the db or an error is encountered before returning a "true" or "false" value back to the client/Postman
	// The "then" method will accept and store the following 2 arguements
		// The first parameter will storte the result returned by the Mongoose "save" method
		// The second parameter will store the "error" object

	return newTask.save().then((task, error) => {

		// If an error is encountered returns a "false" boolean back to the postman
		if(error){
			console.log(error)
			return false

		// If successful, returns the new task object back to the client/Postman
		} else {
			return task
		};
	});
};

// Controller deleting a task

// The task id retrieved from the "req.params.id" property coming from the client is renamed as a "taskId" parameter in the controller file
module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {

		if(err){
			console.log(err)
			return false

		} else {
			return `"${taskId}" is now already deleted.`
		}
	})
}

// Controller updating a task

module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, err) => {

		if(err){
			console.log(err);
			return false;
		}

		result.name = newContent.name;


		return result.save().then((updatedTask, saveErr) => {

			if(saveErr){

				console.log(saveErr); 
				return false;

			} else {

				return "Task updated.";
			};
		});
	});
};

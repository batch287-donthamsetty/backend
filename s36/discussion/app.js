// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

// This allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require("./routes/taskRoutes")

// Setup the server
const app = express();
const port = 3000;
app.use(express.json());
app.use(express.urlencoded({ extended: true	}));

// Setup the Database

	// Connecting to MongoDB Atlas
	mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.zejnk78.mongodb.net/s36-discussion", 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);

	mongoose.connection.on("error", console.error.bind(console, "connection error"));

	mongoose.connection.once("open", () => console.log("We're connected to the cloud database!"));

	// Server listening
	app.listen(port, () => console.log(`Currently listening to port ${port}`));

// Add the tasks route
// Allow us to all the task routes created in "taskRoute.js" file to use "/tasks" route
	// http://localhost:3000/tasks
	app.use("/tasks", taskRoute);
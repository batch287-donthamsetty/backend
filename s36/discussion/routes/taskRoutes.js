// Defines WHEN particular controller will be used
// Contains all the endpoints for our application

// We separate routes such that "app.js" only contains information on the server

// We will need to use express Router() function to achieve this
const express = require("express");

// Creates a Router instance that function as a middleware and routing system
const router = express.Router();

const taskController = require("../controller/taskController")

// [ SECTION ] Routes

// Routes to get all the tasks
router.get("/", (req, res) => {

	// Invoke the "getAllTasks()" function from the "controller.js" file and return the result back to the client/Postman

	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// Route to create a new task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

// Delete task

router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));

});

// Updating a task

/*
Business Logic
1. Get the task with the id using the Mongoose method "findById"
2. Replace the task's name returned from the database with the "name" property from the request body
3. Save the task
*/

router.put("/:id", (req,res) => {

	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));

});



module.exports = router;
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	
	function printWelcomeMessage(){
		let fullName = prompt("Enter your Full Name");
		let age = prompt("Enter your Age");
		let location = prompt("Enter your Location");

		console.log("Hello, " + fullName );
		console.log("You are " + age + " years old. ");
		console.log("you live in " + location + " City ");

		alert("Thank you!");

	};

	printWelcomeMessage();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

		function displayFavBands(){
		console.log('1. The Beatles');
		console.log('2. Metallica');
		console.log('3. The Eagles');
		console.log("4. L'arc~en~Ciel");
		console.log('5. Eraserheads');
	};

	displayFavBands();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function get(){
		let movieName1 = "1. The Godfather";
		console.log(movieName1);
		console.log("Rotten Tomatoes Rating: 97%");
	};

	get();

	function getcourse(){
		let movieName2 = "2. The Godfather, Part II";
		console.log(movieName2);
		console.log("Rotten Tomatoes Rating: 96%");
	};

	getcourse();

	function and(){
		let movieName3 = "3. Shawshank Redemption";
		console.log(movieName3);
		console.log('Rotten Tomatoes Rating: 91%');
	};

	and();

	function be(){
		let movieName4 = "4. To kill A Mockingbird";
		console.log(movieName4);
		console.log('Rotten Tomatoes Rating: 93%');
	};

	be();

	function did(){
		let movieName5 = "5. Psycho";
		console.log(movieName5);
		console.log('Rotten Tomatoes Rating: 96%');
	};

	did();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/



	function printFriends(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();



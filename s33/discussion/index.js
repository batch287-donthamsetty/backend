console.log("Hellow Wurld?");

// [ SECTION ] Javascript Sychronous vs Asynchronous
	// Javascript is by default is a synchronous, meaning that only one statement is executed at a time.

// This can be proven when a statement has an error, JS will not proceed with the next statement
	console.log('Hellow World');
	// console.log("Hellow again world");
	console.log("again world hellow");

	// for(let i = 0; i <= 1500; i++){
	// 	console.log(i);
	// };

	console.log("I'min 1501");

	// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background

	// [ SECTION ] Getting ALL Post

	// The FETCH API allows us to asynchronous request for a resources (data)
	// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value

	// Syntax:
		// fetch("URL")
		console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

	// Syntax:
		// fetch("URL")
		// .then((response) => {})

		fetch('https://jsonplaceholder.typicode.com/posts')
		.then(response => console.log(response.status));

		// The "fetch" method will return a "promise" that resolves to a "response" object
		// The "then" method captures the "response" object and returns another "promise" which will eventually be "resolved" or "rejected".

	fetch('https://jsonplaceholder.typicode.com/posts')
		// Use the "json" method from the "Response" object to convert data retrieved into JSON format to be used in our application
	.then((response) => response.json())
		// Print the converted JSON value from the "fetch" request
		// Using multiple "then" methods creates a "promise chain"
	.then((json) => console.log(json));

	// The "async" and "await" keywords is another approach that can be used to achieve asynchronous code
	// Used in function to indicate which portions of code should be waited for
	// Creates an asynchronous function

	async function fetchData(){

		let result = await fetch("https://jsonplaceholder.typicode.com/posts");

		console.log(result);

		let json = await result.json();

		console.log(json);
	};

	fetchData();

	console.log("Running!");

	// [ SECTION ] Getting a specific post
		// Retrieves a specific post following the REST API (retrieve, /posts/:id,GET)

	fetch("https://jsonplaceholder.typicode.com/posts/1")
	.then((response) => response.json())
	.then((json) => console.log(json))

// [ SECTION ] Creating a post

	// Syntax:
		// fetch("URL", options)
		// .then((response) => {})
		// .then((response) => {})

	// Creates a new post following the REST API (create, /post/:id, POST)

	fetch("https://jsonplaceholder.typicode.com/posts", {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: "New Post",
			body: "Hellow World?!",
			userId: 1,
		})
	})

	.then((response) => response.json())
	.then((json) => console.log(json))

// [ SECTION ] Updating a post using PUT Method
	// Updates a specific post following the REST API (update, /post/:id, PUT)

	fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: "New Post",
			body: "Hellow Again World?!",
			userId: 1,
		})
	})

	.then((response) => response.json())
	.then((json) => console.log(json))

// [ SECTION ] Updating a post using PATCH Method
	// Updates a specific post following the REST API (update, /posts/:id, PATCH)

	fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: "Corrected Post",
		})
	})

	.then((response) => response.json())
	.then((json) => console.log(json));

	// PATCH is used to update a single/several properties
	// PUT is used to update the whole object

// [ SECTION ] Deleting a post
	// Deleting a specific post following the REST API (delete, /posts/:id, DELETE)
fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method: 'DELETE',
	});

// [ SECTION ] Retrieving nested/related comments to posts
	// Retrieving comments for a specific post following the REST API (retrieve, /posts/:id/comments, GET)	

	fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method: 'GET',
	})
	.then((response) => response.json())
	.then((json) => console.log(json))


console.log("Hellow Wurld?");

let studentNumberA = '2020-1923';
let studentNumberB = '2020-1924';
let studentNumberC = '2020-1925';
let studentNumberD = '2020-1926';
let studentNumberE = '2020-1927';

// But with arrays, we can simply write our code above like this:
let studentNumbers = [ '2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927' ]

console.log(studentNumbers);

// [ SECTION ] Arrays
	
	// Arrays are used to store multiple related values in single variable
	// We declare arrays using square brackets ([]) also known as "Array Literals"

	// Common Examples of Array
	let grades = [ 98.5, 94.5, 78.7, 88.1 ];
	let computerBrands = [ 'Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

	// Possible use of an arreay but is not recommended
	let mixedArr = [ 12, 'Asus', true, null, undefined, {} ];

	console.log(grades);
	console.log(computerBrands);
	console.log(mixedArr);

	// Alternative way to write arrays

	let myTasks = [
		'drink HTML',
		'eat javascript',
		'inhale css',
		'bake bootstrap'
	];

	// Using a variable

	let city1 = 'Tokyo';
	let city2 = 'Manila';
	let city3 = 'Jakarta';

	let cities = [ city1, city2, city3 ];

		console.log(myTasks);
		console.log(cities);

// [ SECTION ] Length Property

	// The .length property allows us to get and set the total number of items in array

		console.log(myTasks.length);
		console.log(cities.length);

		let blankArr = [];
		console.log(blankArr.length);

		// Length property can also be used with string. Some array methods and properties can also be used with strings.
		let fullName = 'Jaime Noble';
		console.log(fullName.length);

		// Length property can also set the total number of items in an array, meaning we can actually delete the last item in the array or shorten the array by simple updating the length property of an array.

		myTasks.length = myTasks.length-1;
		console.log(myTasks.length);
		console.log(myTasks);

		// To delete a specific item in an array we can employ array methods (which will be shown in the next session) or an algorithm (set of code to process tasks).

		// Example using Decrementation
		cities.length--
		console.log(cities);

		// We can't do the same on strings
		fullName.length = fullName.length-1
		console.log(fullName.length);
		fullName.length--;
		console.log(fullName);

		let theBeatles = [ 'John', 'Paul', 'Ringo', 'George'];
		console.log(theBeatles);
		theBeatles.length++
		console.log(theBeatles);
// [ SECTION ] Reading from Arrays

	// Accessing array elements is one of the more common tasks that we do with an array

	// Syntax:
		// arrayName[index]:
		console.log(grades[3]);
		console.log(computerBrands[2]);
		console.log(grades[100]);

		let lakersLegends = [ 'Kobe', 'Shaq', 'LeBron', 'Magic', 'Kareem' ];

		console.log(lakersLegends[1]); // Will print Shaq
		console.log(lakersLegends[3]); // Will print Magic

		// We can also save/store array items in another variable.
		let currentLaker = lakersLegends[2];
		console.log(currentLaker); // Will print Lebron

		console.log('Array before reassignment');
		console.log(lakersLegends);
		lakersLegends[2] = 'Pau Gasol';
		console.log('Array after reassignment');
		console.log(lakersLegends);

		// Accessing the last element of an array
		// Since the first element of an array starts at 0, subtracting 1 to the length of an array will offset the value by one allowing us to get the last element

		let bullsLegends = [ 'Jordan', 'Pippen', 'Rodman', 'Rose', 'Kukoc'];
		console.log(bullsLegends);

		let lastElementIndex = bullsLegends.length - 1;

		console.log(bullsLegends[lastElementIndex]);

		// You can also add it directly:
		console.log(bullsLegends[bullsLegends.length-1]);

		// Adding Items into the Array

		let newArr = []
		console.log(newArr[0]); // undefined

		newArr[0] = 'Cloud Strife';
		console.log(newArr);

		console.log(newArr[1]);
		newArr[1] = 'Tifa Lockhart';

		console.log(newArr);

		// newArr[newArr.length-1] = 'Barrett Walace'
		newArr[newArr.length] = 'Barrett Walace';
		console.log(newArr)

// Looping Over an Array
	// We can use For Loop to iterate overall items in an array.

		for(let index = 0; index < newArr.length; index++){

			//You can use the loop counter as index to be able to show each array items in a console.log.
			console.log(newArr[index]);
		};


	let numArr = [ 5, 12, 69, 87, 765 ];

	for(let index =0; index < numArr.length; index++){
		
		if(numArr[index] % 5 === 0){
			
			console.log(numArr[index] + " is divisible by 5");

		} else{
		
		console.log(numArr[index] + " is not divisible by 6");
	}
}

// [ SECTION ] Multidimensional Arrays

	// Multidimensional Arrays are useful for storing complex data structures

	let chessBoard = [
		[ 'a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
		[ 'a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
		[ 'a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
		[ 'a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
		[ 'a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
		[ 'a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
		[ 'a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
		[ 'a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8'],

	];

	console.log(chessBoard);

	// Accessing elements of a multidimensional arrays
	console.log(chessBoard[1][7]);

	console.log('Pawn moves to: ' + chessBoard[1][5]);
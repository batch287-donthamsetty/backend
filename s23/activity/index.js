let info = {
		name: "Ash Ketchum",
		age: 10,
		pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
		friends: {
			hoenn: ["May", "Max"],
			kanto: ["Brock", "Misty"]
		},
		talk: function(){
			console.log('Pikachu! I choose you! ');
		}
	};
	
	console.log(info);
	console.log(typeof info);

	console.log('Result of dot notation: ');
	console.log(info.name);

	console.log('Result of square bracket notation: ');
	console.log(info.pokemon);

	console.log('Result of talk method')
	info.talk();
	
	function pokemon (name, level){

		// Properties
		this.name = name;
		this.level = level;
		this.health = 2*level;
		this.attack = level;

		// Methods
		this.faint = function(target){
			console.log(target.name + ' fainted ')
		};
		this.tackle = function(target){
		console.log(this.name +" tackled "+ target.name)
		target.health = target.health - this.attack;
		console.log(target.name + "'s health is name reduced to "+ target.health);
		
		if( target.health <= 0){
			this.faint(target);
		}
	};
	};


	let pikachu = new pokemon("pikachu", 12);
	console.log(pikachu);

	let geodude = new pokemon("Geodude", 8);
	console.log(geodude);

	let mewtwo = new pokemon("Mewtwo", 100);
	console.log(mewtwo);

	geodude.tackle(pikachu);
	console.log(pikachu);

	mewtwo.tackle(geodude);
	console.log(geodude);
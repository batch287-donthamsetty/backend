console.log("Hellow Wurld?");

// [ SECTION ] Objects
	// An object is a datatype that is used to represent real world objects
	// Information stored in objects are represented in a "key:value" pair
	
	// Creating object using object initializers/literal notation
	
	// Syntax:
		// let objectName = {
			// keyA: valueA,
			// keyB: valueB
		//}
		
	let cellPhone = {
		name: 'Nokia 3210',
		manufactureDate: 1999,
	};

	console.log('Creating object using initializers/literal notation:');
	console.log(cellPhone);
	console.log(typeof cellPhone);

	// Creating object using a constructor function

	// Creates a reusable function to create several object that have the same data structure

	// Syntax:
		// function objectName(keyA, keyB){
			// this.keyA = KetA,
			// this.keyB = KetB
		//}
	// This is an object
	// The "this" keyword allows us to assign a new object's properties by associating them with values received from a constructor function's parameters

function Shirt (color, day){
	this.colorSelected = color;
	this.daySelected = day;
};

// The "new" operator creates an instance of an object

let shirt = new Shirt('Red', "Monday");
console.log("Result of Creating object using a constructors: ");
console.log(shirt);

let dress = new Shirt('Blue', "Tuesday");
console.log("Result of Creating object using a constructors: ");
console.log(dress);

let socks = new Shirt('Yellow', "Thursday");
console.log("Result of Creating object using a constructors: ");
console.log(socks);

	// Unable call that new operators it will return undefined without the 'new' operator
	// The behaviour for this is lika calling/invoking the Laptop function instead of creating a new object instance.

// Create empty objects
let computer = {};
let myComputer = new Object();

// [ SECTION ] Accessing Object Properties

// Using the dot notation
console.log('Result from dot notation: ' + shirt.colorSelected);

// Using the square bracket notation
console.log('Result from square bracket notation: ' + shirt['daySelected']);

// Accessing Array Objects
	 
	// Accessing array elements can be also done using square brackets

let array = [ shirt, dress];
console.log(array);

console.log(array[0]['daySelected']);
// This tells us that array[0] is an object by using the dot notation
console.log(array[0].daySelected);

// Initializing/Adding object properties using dot notation

let car = {};

car.name = 'Honda Civic';
console.log("Result from Adding object properties using dot notation");
console.log(car);

// Initializing/Adding object properties using bracket notation

car['manufacture date'] = 2023;
console.log(car['manufacture date']);
console.log(car['manufacture Date']);
console.log(car.manufacturedate);
console.log('Result from Adding object properties using bracket notation');
console.log(car);

// Deleting Object Properties
delete car['manufacture date'];
console.log('Result from deleting properties');
console.log(car);

// Reassinging object properties
car.name = 'Dodge Charger R/T';
console.log('Result from Reassining properties');
console.log(car);

// [ SECTION ] Object Methods
	// A method is a function which is a property of an object

	let person = {
		name: 'John',
		talk: function(){
			console.log('Hello my name is ' + this.name);
		}
	};

	console.log(person);
	console.log('Result from object method: ');
	person.talk();

	// Methods are useful for creating reusable functions that perform tasks related to object

	let friend = {
		firstName: 'Joe',
		lastName: 'Smith',
		address: {
			city: "Austin",
			country: 'Texas'
		},
		email: ['joe@mail,com', 'joesmith@mail.xyz'],
		introduce: function(){
			console.log('Hellow my name is ' + this.firstName + " " + this.lastName);
		},
		introduceAddress: function(){
			console.log('I currently live in ' + this.address.city + "," + this.address.country + "!");
		},
		introduceContact: function(){
			console.log('My email address follows: ' + this.email[0] + "," + this.email[1]);
		}
	};

	friend.introduce();
	friend.introduceAddress();
	friend.introduceContact();

	// [ SECTION ] Real World Application of Objects

		/*
		Scenario
			1. We would like to create a game that would have several pokemon interact with each other 
			2. Every pokemon would have the same set of stats, properties and functions
		*/
	// Using objects literals to create multiple kinds of pokemon would be time consuming

	let myPokemon = {
		name: 'Pikachu',
		level: 3,
		health: 100,
		attack: 50,
		tackle: function(){
			console.log("This pokemon tacked targetPokemon");
			console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");
		},
		faint: function(){
			console.log('Pokemon fainted!');
		}
	}

	console.log(myPokemon);

	function pokemon (name, level){

		// Properties
		this.name = name;
		this.level = level;
		this.health = 2*level;
		this.attack = level;

		// Methods	
		this.tackle = function(target){
			console.log(this.name + " tackled " + target.name)
			console.log(target.name + " health is now reduced to " + this.health);
		};
		this.faint = function(){
			console.log(this.name + ' fainted');
		};
	};

	let pikachu = new pokemon("pikachu", 16);
	let rattata = new pokemon("Rattata", 8);

	pikachu.tackle(rattata);
	rattata.faint();